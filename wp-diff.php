<?php
/**
 * WP Diff plugin for WordPress
 *
 * Plugin Name:  WP Diff - Visual Regression Testing for WordPress
 * Description:  Catch unexpected issues and changes to your website before your customers do, using our automated visual change detection service.
 * Version:      0.0.1
 * Plugin URI:   https://wpdiff.com/
 * Author:       WildPress
 * Author URI:   https://wildpress.co/
 * Text Domain:  wpdiff
 * Domain Path:  /languages/
 * Requires PHP: 7.2
 *
 * @package WPDiff
 */

define( 'WPDIFF__MINIMUM_WP_VERSION', '5.2' );
define( 'WPDIFF__MINIMUM_PHP_VERSION', '7.2' );
define( 'WPDIFF__VERSION', '0.0.1' );
define( 'WPDIFF__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPDIFF__PLUGIN_FILE', __FILE__ );
define( 'WPDIFF__UPLOAD_DIR', wp_get_upload_dir()['basedir'] . '/wpdiff/' );

//TODO: Validate WordPress version is supported

require_once WPDIFF__PLUGIN_DIR . 'vendor/autoload.php';
require_once WPDIFF__PLUGIN_DIR . 'inc/class-init.php';

register_activation_hook( __FILE__, [ 'WPDiff\WPDiff', 'activate' ] );
register_activation_hook( __FILE__, [ 'WPDiff\WPDiff', 'deactivate' ] );

