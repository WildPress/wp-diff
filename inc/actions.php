<?php

namespace WPDiff;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

add_action( 'admin_menu', [ __NAMESPACE__ . '\\Admin', 'add_submenu_page' ] );
