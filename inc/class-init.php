<?php
/**
 * Load WP Diff files
 *
 * @package WPDiff
 */

namespace WPDiff;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

final class WPDiff {
	protected static $instance = null;

	protected function __construct() {
	}

	static public function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		self::$instance->init();

		return self::$instance;
	}

	public function activate() {
		$version = get_option( 'wpdiff_version' );
		if ( ! $version ) {
			update_option( 'wpdiff_version', WPDIFF__VERSION );
		}
	}

	public function deactivate() {
	}

	public function init() {
		require_once WPDIFF__PLUGIN_DIR . 'inc/actions.php';

		if ( is_admin() ) {
			require_once WPDIFF__PLUGIN_DIR . 'inc/class-admin.php';
		}
	}
}

function wpdiff() {
	return WPDiff::instance();
}

add_action( 'plugins_loaded', __NAMESPACE__ . '\\wpdiff' );
