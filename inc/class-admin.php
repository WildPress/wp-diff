<?php

namespace WPDiff;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

final class Admin {

	public static function admin_styles() {
		wp_enqueue_style( 'wpdiff', plugin_dir_url( WPDIFF__PLUGIN_FILE ) . 'dist/wp-diff.css', [], false );
	}

	public static function admin_scripts() {
		wp_enqueue_script( 'wpdiff', plugin_dir_url( WPDIFF__PLUGIN_FILE ) . 'dist/wp-diff.js', [], false, true );
	}

	public static function add_submenu_page() {
		$submenu = add_submenu_page( 'tools.php',
			__( 'WP Diff', 'wpdiff' ),
			__( 'WP Diff', 'wpdiff' ),
			'edit_themes',
			'wpdiff',
			[ __NAMESPACE__ . '\\Admin', 'display_control_panel' ] );

		add_action( 'admin_print_styles-' . $submenu, [ __NAMESPACE__ . '\\Admin', 'admin_styles' ] );
		add_action( 'admin_print_scripts-' . $submenu, [ __NAMESPACE__ . '\\Admin', 'admin_scripts' ] );
	}

	public static function display_control_panel() {
		include WPDIFF__PLUGIN_DIR . 'templates/admin.php';
	}
}